extern crate termion;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::screen::*;
use std::io::{Write, stdout, stdin};

fn write_buffer<W: Write>(screen: &mut W, buffer: Buffer){
    write!(screen, "{}{}{}",
           termion::clear::All,
           termion::cursor::Goto(1,1),
           buffer.content).unwrap();
}

fn main() {
    let stdin = stdin();
    let mut screen = AlternateScreen::from(stdout().into_raw_mode().unwrap());
    // write!(screen, "{}", termion::cursor::Hide).unwrap();
    // write_alt_screen_msg(&mut screen);

    screen.flush().unwrap();

    let mut current_state = State::Layout;

    for c in stdin.keys() {
        match current_state {
            State::Layout => match c.unwrap() {
                Key::Char('q') => break,
                Key::Esc => {
                    current_state = State::Edit;
                }
                Key::Char('i') => {
                    current_state = State::Insert;
                }
                Key::Char('r') => {
                    write!(screen, "{}", ToAlternateScreen).unwrap();
                    write_buffer(&mut screen, Buffer::new());
                }
                _ => {}
            }
            State::Edit => match c.unwrap() {
                Key::Char('`') => {
                    current_state = State::Layout;
                }
                Key::Char('i') => {
                    current_state = State::Insert;
                }
                _ => {}
            }
            State::Insert => match c.unwrap() {
                Key::Esc => {
                    current_state = State::Edit;
                }
                Key::Char(_) => {
                    //Write to cursor???
                }
                _ => {}
            }
            State::Visual => match c.unwrap() {
                _ => {}
            }
            State::Command => match c.unwrap() {
                _ => {}
            }
        }
        screen.flush().unwrap();
    }
    write!(screen, "{}", termion::cursor::Show).unwrap();
}

struct Buffer {
    // termion_buffer: AlternateScreen,
    content: String, //@SPEED(renzix): Make this a byte buffer or something???
    major_mode: Mode,
    minor_mode: Vec<Mode>,
}

impl Buffer {
    fn new() -> Buffer {
        let termion_buffer = AlternateScreen::from(stdout().into_raw_mode().unwrap());
        Buffer {
            // termion_buffer,
            content: String::new(),
            major_mode: Mode::new("Fundemental".to_string()),
            minor_mode: Vec::new(),
        }
    }
}

struct Mode {
    name: String,
}

impl Mode {
    fn new(name: String) -> Mode {
        Mode {
            name: String::new(),
        }
    }
}

enum State {
    Layout,
    Edit,
    Insert,
    Visual,
    Command,
}
